<?php

function find(mixed $id, array $array, bool $strict = false): bool {
    if ($strict) {
        foreach($array as $value) {
            if ($id === $value) {
                $i = 1;
                return $i;
            }
            $i = 0;
        }
    } else {
        foreach($array as $value) {
            if ($id == $value) {
                $i = 1;
                return $i;
            }
            $i = 0;
        }
    }  
    return $i;
}







$a = [['', 'h'], ['p', 'r'], 'o'];

if (find(['p', 'h'], $a)) {
    echo "'ph' найдено\n";
}

$os = ["Mac", "NT","Irix", "Linux"];

if (find("Irix", $os)) {
    echo "Нашёл Irix";
}
?>