<?php
$records = [
    [
        'id' => 2135,
        'first_name' => 'John',
        'last_name' => 'Doe',
    ],
    [
        'id' => 3245,
        'first_name' => 'Sally',
        'last_name' => 'Smith',
    ],
    [
        'id' => 5342,
        'first_name' => 'Jane',
        'last_name' => 'Jones',
    ],
    [
        'id' => 5623,
        'first_name' => 'Peter',
        'last_name' => 'Doe',
    ],
];
function array_ultra(array $array, int|string|null $keyword, int|string|null $idol = null ) : array { 
    $result = [];
    foreach ($array as $value) {
        foreach ($value as $key => $valuex) {
            if ($key === $keyword) {
                if ($idol != null) {
                    $result[$value[$idol]] = $valuex;
                } else {
                    $result[] = $valuex;
                }
                break;
            }
        }
    }
    return $result;
}



$first_names = array_ultra($records,'id','last_name');
var_dump($first_names);
echo "<br>";
var_dump(array_ultra($records,'id'));
echo "<br>";
var_dump(array_column($records,'id'));
echo "<br>";
$first_names = array_column($records,'id','last_name');
var_dump($first_names);
?>