<?php


function sum(array $array): int|float {
    $sum = 0;
    foreach($array as $value) {
        if (gettype($value) == 'string')
            $value = floatval($value);
        $a = ['integer','double','boolean'];
        if (in_array(gettype($value),$a))
            $sum += $value;
    }
    return $sum;
}

$a = array(2, 'sfscd 100', 6, 15);
echo "sum(a) = " . sum($a) . "\n" ;
echo "<br>";

$b = array("a" => '1221363', "b" => 2.5, "c" => 3.4);
echo "sum(b) = " . sum($b) . "\n";
echo "<br>";

echo array_sum($b);
echo "<br>";
echo array_sum($a);


?>