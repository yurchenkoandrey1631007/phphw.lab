<?php


function flip(array $array): array {
    $i = [];
    foreach($array as $key => $value) {
        if ((gettype($value) == 'string') or (gettype($value) == 'integer')) {
            $i[$value] = $key;
        } else {
            trigger_error("<br>Warning: array_flip(): Can only flip string and integer values<br>");
        }
    }
    return $i;
}


$input = array("wtf", null, [1,2,3], 6.6, "pears");
$flipped = flip($input);
print_r($flipped);
// $flipped = array_flip($input);
// echo "<br>";
// print_r($flipped);
?>